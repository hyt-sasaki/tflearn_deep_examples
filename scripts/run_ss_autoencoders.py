# -*- coding:utf-8 -*-
## @package example
#
#  実験用スクリプトの具体例が記述されているパッケージ
from exp_wrapper import template
from argparse import ArgumentDefaultsHelpFormatter
import argparse

from tf_deep.ss_autoencoders import SSAutoEncoders
import numpy as np
from tf_deep.util import get_activation_by_name


## 実験用スクリプトのメインとなるクラス
#
#  実験用スクリプトのメインとなるクラスで, template.Mainを継承する.
class Main(template.Main):        # templateを継承
    ## コンストラクタ
    #  @param self オブジェクト自身に対するポインタ
    #  @param argv コマンドライン引数を保持するリスト
    def __init__(self, argv):
        # 親クラスtemplate.Mainのコンストラクタを実行
        model_parser = self.make_model_parser()
        model_args, remain_args = model_parser.parse_known_args(argv)
        input_parser = self.make_input_parser()
        input_args, remain_args = input_parser.parse_known_args(remain_args)
        super(Main, self).__init__(remain_args)
        setattr(self.args, 'model_args', model_args)
        setattr(self.args, 'input_args', input_args)

    ## メイン処理を行うメソッド
    #
    # 実験用スクリプトのメイン処理を行うメソッド.
    # 基本的な処理内容は本メソッドに記述する.
    def execute(self):
        setattr(self.args.model_args, 'graph', None)
        self.logger.info(self.args)

        X_list = self.args.input_args.trainX_list
        self.args.model_args.loss = \
            get_activation_by_name(self.args.model_args.loss)
        self.logger.info(self.args)

        for (n_input, X) in zip(
            self.args.model_args.n_input_list, self.args.input_args.trainX_list
        ):
            assert n_input == X.shape[1]

        self.model = SSAutoEncoders(**vars(self.args.model_args))

        validX_list = self.args.input_args.validX_list

        try:
            self.model.fit(
                X=X_list, y=X_list, validation_set=(validX_list, validX_list),
                restore=self.args.restore
            )
        finally:
            self.save()

    def save(self):
        testX_list = self.args.input_args.testX_list
        validX_list = self.args.input_args.validX_list

        def save_outputs(X_list, mode):
            if X_list is not None:
                f_list = self.model.encode(X_list)
                r_list = self.model.predict(X_list)
                for m, (r, f) in enumerate(zip(r_list, f_list)):
                    r_fname = 'reconstruction_%s_%d.npy' % (mode, m)
                    f_fname = 'encoded_%s_%d.npy' % (mode, m)
                    np.save(r_fname, r)
                    self.out_files['Reconstructed(%s)%d' % (mode, m)] = r_fname
                    np.save(f_fname, f)
                    self.out_files['Feature(%s)%d' % (mode, m)] = f_fname

        save_outputs(testX_list, 'test')
        save_outputs(validX_list, 'validation')

        self.out_files['tensorboard_logs'] = self.args.model_args.logdir
        self.in_params['input_args'] = vars(self.args.input_args)
        self.in_params['model_args'] = vars(self.args.model_args)

    ## コマンドライン引数のパーサを生成するメソッド
    #
    #  コマンドライン引数のパーサを生成するメソッドで,
    #  必要に応じたコマンドライン引数を設定する.
    #  @param self オブジェクト自身に対するポインタ
    #  @return parser パーサオブジェクト
    def make_parser(self):
        parser = super(Main, self).make_parser()
        # parserの設定
        parser_decsription = """
        Strongly Shared DeepAutoEncoder.
        """
        parser.description = parser_decsription
        parser.prog = 'ss_deep_autoencoder'
        parser.formatter_class = ArgumentDefaultsHelpFormatter

        restore_help = 'flag for whether to restore the previous model'
        parser.add_argument(
            '--restore',
            type=bool,
            nargs='?',
            default=False,
            help=restore_help
        )

        return parser

    def make_model_parser(self):
        # parserの設定
        parser_decsription = """
        parser for model parameters.
        """
        parser = argparse.ArgumentParser(
            description=parser_decsription
        )
        parser.formatter_class = ArgumentDefaultsHelpFormatter
        n_input_help = 'number of neuron for input layer'
        parser.add_argument(
            '--n_input_list',
            type=int,
            nargs='+',
            required=True,
            help=n_input_help
        )
        layers_help = 'num of neuron per layer for each modality'
        parser.add_argument(
            '--layers_list',
            type=int,
            nargs='+',
            action='append',
            required=True,
            help=layers_help
        )
        optimizer_help = 'name of optimizer for training'
        parser.add_argument(
            '--optimizer',
            type=str,
            default='adam',
            help=optimizer_help
        )
        learning_rate_help = 'learning rate'
        parser.add_argument(
            '--learning_rate',
            type=float,
            default=1e-2,
            help=learning_rate_help
        )
        batch_size_help = 'batch size'
        parser.add_argument(
            '--batch_size',
            type=int,
            default=100,
            help=batch_size_help
        )
        loss_help = 'name of loss function'
        parser.add_argument(
            '--loss',
            type=str,
            default='mean_square',
            help=loss_help
        )
        n_epoch_help = 'number of epoch for training'
        parser.add_argument(
            '--n_epoch',
            type=int,
            default=1,
            help=n_epoch_help
        )
        batch_normalization_help = 'flag for wheter to use batch normalization'
        parser.add_argument(
            '--batch_normalization',
            type=bool,
            nargs='?',
            default=False,
            help=batch_normalization_help
        )
        tied_weight_help = 'flag for whether to use tied weights'
        parser.add_argument(
            '--tied_weight',
            type=bool,
            nargs='?',
            default=False,
            help=tied_weight_help
        )
        encoder_act_help = 'name of activation for encoder layers'
        parser.add_argument(
            '--encoder_act_lists',
            type=str,
            nargs='+',
            action='append',
            help=encoder_act_help
        )
        decoder_act_help = 'name of activation for decoder layers'
        parser.add_argument(
            '--decoder_act_lists',
            type=str,
            nargs='+',
            action='append',
            help=decoder_act_help
        )
        name_help = 'name of Deep AutoEncoder network'
        parser.add_argument(
            '--name',
            type=str,
            default='SSAE',
            help=name_help
        )
        logdir_help = 'log directory name for tensorboard'
        parser.add_argument(
            '--logdir',
            type=str,
            default='./tensorboard_dir',
            help=logdir_help
        )
        tensorboard_verbose_help = \
            'verbosity level for tensorboard visualization'
        parser.add_argument(
            '--tensorboard_verbose',
            type=int,
            default=3,
            help=tensorboard_verbose_help
        )
        seed_help = 'seed for tensorflow and numpy'
        parser.add_argument(
            '--seed',
            type=int,
            default=None,
            help=seed_help
        )
        memory_fraction_help = \
            'fraction of the available GPU memory' \
            'to pre-allocate for each process.'
        parser.add_argument(
            '--memory_fraction',
            type=float,
            default=0.3,
            help=memory_fraction_help
        )

        return parser

    def make_input_parser(self):

        def npType(string):
            ft = argparse.FileType('r')
            f = ft(string)
            return np.load(f)

        # parserの設定
        parser_decsription = """
        parser for model parameters.
        """
        parser = argparse.ArgumentParser(
            description=parser_decsription
        )
        parser.formatter_class = ArgumentDefaultsHelpFormatter
        train_data_help = 'training data file(.npy)'
        parser.add_argument(
            '--trainX_list',
            type=npType,
            required=True,
            nargs='+',
            help=train_data_help
        )
        test_data_help = 'test data file(.npy)'
        parser.add_argument(
            '--testX_list',
            type=npType,
            default=None,
            nargs='+',
            help=test_data_help
        )
        validation_data_help = 'validation data file(.npy)'
        parser.add_argument(
            '--validX_list',
            type=npType,
            default=None,
            nargs='+',
            help=validation_data_help
        )

        return parser


# experiment.pyを用いずに, 本スクリプトを直接実行する場合は
# こちらが実行される
if __name__ == '__main__':
    import sys
    obj = Main(sys.argv[1:])
    template.main(obj)
