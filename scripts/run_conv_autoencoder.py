# -*- coding:utf-8 -*-
## @package example
#
#  実験用スクリプトの具体例が記述されているパッケージ
from exp_wrapper import template
from argparse import ArgumentDefaultsHelpFormatter
import argparse
from functools import partial, reduce
from operator import mul

from tf_deep.conv_ae import ConvAutoencoder
import numpy as np
import matplotlib.pyplot as plt


## 実験用スクリプトのメインとなるクラス
#
#  実験用スクリプトのメインとなるクラスで, template.Mainを継承する.
class Main(template.Main):        # templateを継承
    ## コンストラクタ
    #  @param self オブジェクト自身に対するポインタ
    #  @param argv コマンドライン引数を保持するリスト
    def __init__(self, argv):
        # 親クラスtemplate.Mainのコンストラクタを実行
        model_parser = self.make_model_parser()
        model_args, remain_args = model_parser.parse_known_args(argv)
        input_parser = self.make_input_parser()
        input_args, remain_args = input_parser.parse_known_args(remain_args)
        super(Main, self).__init__(remain_args)
        setattr(self.args, 'model_args', model_args)
        setattr(self.args, 'input_args', input_args)

    ## メイン処理を行うメソッド
    #
    # 実験用スクリプトのメイン処理を行うメソッド.
    # 基本的な処理内容は本メソッドに記述する.
    def execute(self):
        setattr(self.args.model_args, 'graph', None)
        self.logger.info(self.args)

        X = self.args.input_args.trainX
        validX = self.args.input_args.validX

        n_input = partial(reduce, mul)(self.args.model_args.image_shape)

        assert n_input == X.shape[1]

        shape = self.args.model_args.image_shape
        if len(shape) == 2:
            shape.append(1)
        X = X.reshape([-1, ] + shape)
        if validX is not None:
            validX = validX.reshape([-1, ] + shape)

        self.model = ConvAutoencoder(**vars(self.args.model_args))

        try:
            self.model.fit(
                X=X, y=X, validation_set=(validX, validX),
                restore=self.args.restore
            )
        except:
            import traceback
            print traceback.format_exc()
        finally:
            self.save()

    def save(self):
        testX = self.args.input_args.testX
        shape = self.args.model_args.image_shape
        if len(shape) == 2:
            shape.append(1)
        if testX is not None:
            testX = testX.reshape([-1, ] + shape)

        def save_outputs(X):
            if X is not None:
                r = self.model.predict(X)
                f = self.model.encode(X)
                r_fname = 'reconstruction.npy'
                f_fname = 'encoded.npy'
                np.save(r_fname, r)
                self.out_files['Reconstructed'] = r_fname
                np.save(f_fname, f)
                self.out_files['Feature'] = f_fname

        save_outputs(testX)
        self.model.create_embedding(
            testX,
            self.args.input_args.metadata,
            self.args.input_args.sprite
        )
        loss_arrays = self.model.calc_loss(testX)
        fig = plt.figure()
        ax = fig.add_subplot(111)
        ax.hist(loss_arrays, bins=100)
        plt.savefig('histogrms.png')

        self.out_files['tensorboard_logs'] = self.args.model_args.logdir
        self.in_params['input_args'] = vars(self.args.input_args)
        self.in_params['model_args'] = vars(self.args.model_args)

    ## コマンドライン引数のパーサを生成するメソッド
    #
    #  コマンドライン引数のパーサを生成するメソッドで,
    #  必要に応じたコマンドライン引数を設定する.
    #  @param self オブジェクト自身に対するポインタ
    #  @return parser パーサオブジェクト
    def make_parser(self):
        parser = super(Main, self).make_parser()
        # parserの設定
        parser_decsription = """
        DeepAutoEncoder.
        """
        parser.description = parser_decsription
        parser.prog = 'deep_autoencoder'
        parser.formatter_class = ArgumentDefaultsHelpFormatter

        restore_help = 'flag for whether to restore the previous model'
        parser.add_argument(
            '--restore',
            type=bool,
            nargs='?',
            default=False,
            help=restore_help
        )

        return parser

    def make_model_parser(self):
        # parserの設定
        parser_decsription = """
        parser for model parameters.
        """
        parser = argparse.ArgumentParser(
            description=parser_decsription
        )
        parser.formatter_class = ArgumentDefaultsHelpFormatter
        image_shape_help = 'height, width, channels'
        parser.add_argument(
            '--image_shape',
            type=int,
            nargs='+',
            required=True,
            help=image_shape_help
        )
        nb_filter_list_help = 'num of filters per layer for cnn layers'
        parser.add_argument(
            '--nb_filter_list',
            type=int,
            nargs='+',
            required=True,
            help=nb_filter_list_help
        )
        filter_size_list_help = 'size of filters per layer for cnn layers'
        parser.add_argument(
            '--filter_size_list',
            type=int,
            nargs='+',
            required=True,
            help=filter_size_list_help
        )
        encoder_act_help = 'name of activation for encoder layers'
        parser.add_argument(
            '--encoder_act_list',
            type=str,
            nargs='+',
            required=True,
            help=encoder_act_help
        )
        decoder_act_help = 'name of activation for decoder layers'
        parser.add_argument(
            '--decoder_act_list',
            type=str,
            nargs='+',
            required=True,
            help=decoder_act_help
        )
        strides_list_help = 'strides list for cnn layers'
        parser.add_argument(
            '--strides_list',
            type=int,
            nargs='+',
            required=True,
            help=strides_list_help
        )
        kernel_size_list_help = 'kernel size list for max pooling'
        parser.add_argument(
            '--kernel_size_list',
            type=int,
            nargs='+',
            required=True,
            help=kernel_size_list_help
        )
        optimizer_help = 'name of optimizer for training'
        parser.add_argument(
            '--optimizer',
            type=str,
            default='adam',
            help=optimizer_help
        )
        learning_rate_help = 'learning rate'
        parser.add_argument(
            '--learning_rate',
            type=float,
            default=1e-3,
            help=learning_rate_help
        )
        batch_size_help = 'batch size'
        parser.add_argument(
            '--batch_size',
            type=int,
            default=100,
            help=batch_size_help
        )
        loss_help = 'name of loss function'
        parser.add_argument(
            '--loss',
            type=str,
            default='mean_square',
            help=loss_help
        )
        n_epoch_help = 'number of epoch for training'
        parser.add_argument(
            '--n_epoch',
            type=int,
            default=1,
            help=n_epoch_help
        )
        name_help = 'name of Deep AutoEncoder network'
        parser.add_argument(
            '--name',
            type=str,
            default='CNNAE',
            help=name_help
        )
        logdir_help = 'log directory name for tensorboard'
        parser.add_argument(
            '--logdir',
            type=str,
            default='./tensorboard_dir',
            help=logdir_help
        )
        tensorboard_verbose_help = \
            'verbosity level for tensorboard visualization'
        parser.add_argument(
            '--tensorboard_verbose',
            type=int,
            default=3,
            help=tensorboard_verbose_help
        )
        seed_help = 'seed for tensorflow and numpy'
        parser.add_argument(
            '--seed',
            type=int,
            default=None,
            help=seed_help
        )
        memory_fraction_help = \
            'fraction of the available GPU memory' \
            'to pre-allocate for each process.'
        parser.add_argument(
            '--memory_fraction',
            type=float,
            default=0.3,
            help=memory_fraction_help
        )

        return parser

    def make_input_parser(self):

        def npType(string):
            ft = argparse.FileType('r')
            f = ft(string)
            return np.load(f)

        # parserの設定
        parser_decsription = """
        parser for model parameters.
        """
        parser = argparse.ArgumentParser(
            description=parser_decsription
        )
        parser.formatter_class = ArgumentDefaultsHelpFormatter
        train_data_help = 'training data file(.npy)'
        parser.add_argument(
            '--trainX',
            type=npType,
            required=True,
            help=train_data_help
        )
        test_data_help = 'test data file(.npy)'
        parser.add_argument(
            '--testX',
            type=npType,
            default=None,
            help=test_data_help
        )
        validation_data_help = 'validation data file(.npy)'
        parser.add_argument(
            '--validX',
            type=npType,
            default=None,
            help=validation_data_help
        )
        metadata_help = 'metadata file(.tsv)'
        parser.add_argument(
            '--metadata',
            type=str,
            default=None,
            help=metadata_help
        )
        sprite_help = 'sprite image file'
        parser.add_argument(
            '--sprite',
            type=str,
            default=None,
            help=sprite_help
        )

        return parser


# experiment.pyを用いずに, 本スクリプトを直接実行する場合は
# こちらが実行される
if __name__ == '__main__':
    import sys
    obj = Main(sys.argv[1:])
    template.main(obj)
