# TFLearn examples
## 本リポジトリの構成

+ 各種DNNクラス
    + tf_deep/model.py
    + tf_deep/deep_autoencoder.py
    + tf_deep/cnn.py
    + tf_deep/ss_autoencoders.py
+ 実験用スクリプト
    + tf_deep/scripts/run_deep_autoencoder.py
    + tf_deep/scripts/run_cnn.py
    + tf_deep/scripts/run_ss_autoencoders.py
+ 訓練/テスト/検査用データ生成スクリプト
    + data/data_gen.py
+ Dockerfile
    + docker/Dockerfile

## 実行環境

+ OS
    + Ubuntu 14.04LTS
+ Python
    + ver2.7.9
+ CUDA
    + ver7.5
+ 依存ライブラリ:
    + TensorFlow 0.10.0
    + TFLearn 0.2.0
+ GPU
    + TitanX(Pascal)


## 利用方法
基本的にはDockerでの利用を想定している。

### Dockerコンテナの起動
```bash
# コンテナの起動(イメージが存在しない場合はイメージのダウンロードも行う)
$ nvidia-docker run -d --name [コンテナ名] -p [jupyter用に利用するポート番号]:8888 -p [TensorBoardように利用するポート番号]:6006 registry.gitlab.com/hyt-sasaki/tflearn_deep_examples
# コンテナの端末にアクセス
$ nvidia-docker exec -it [コンテナ名] bash
```

### スクリプトの実行例

```bash
# gitリポジトリを最新のものに更新
$ cd ~/tf_deep
$ git pull
# 訓練データの作成
$ cd ~/tf_deep/data
$ python data_gen.py
# 使用するGPUの選択
$ export CUDA_VISIBLE_DEVICES=0
# 実験スクリプトの実行(CNN)
$ experiment +r results +v 20 +l exp_log.txt ++dir test \
    ~/tf_deep/tf_deep/scripts/run_cnn.py Main \
    --image_shape 28 28 \
    --n_class 10 \
    --nb_filter_list 32 64 \
    --filter_size_list 3 3 \
    --strides_list 1 1 \
    --kernel_size_list 2 2 \
    --fc_layers 128 256 \
    --fc_activation_list tanh tanh \
    --cnn_activation_list relu relu \
    --batch_size 20 \
    --learning_rate 0.01 \
    --n_epoch 10 \
    --dropout True \
    --keep_prob 0.8 \
    --seed 1 \
    --memory_fraction 0.30 \
    --tensorboard_verbose 0 \
    --logdir tensorboard_dir \
    --trainX ~/tf_deep/data/mnist_training_images.npy \
    --trainY ~/tf_deep/data/mnist_training_labels.npy \
    --testX ~/tf_deep/data/mnist_test_images.npy \
    --testY ~/tf_deep/data/mnist_test_labels.npy \
    --validX ~/tf_deep/data/mnist_validation_images.npy \
    --validY ~/tf_deep/data/mnist_validation_labels.npy

# TensorBoardの起動
# 起動後ブラウザからTensorBoardにアクセス
# ホストのIPアドレスが192.168.xxx.yyy, コンテナ起動時に指定したTensorBoard用のポート番号がzzzの場合
# ブラウザにhttp://192.168.xxx.yyy:zzzと入力
$ tensorboard --logdir=~/tf_deep/tf_deep/data/results/run_cnn.Main/test/output_files/tensorboard_dir
#
# 実験スクリプトの実行(Deep Autoencoder)
$ experiment +r results +v 20 +l exp_log.txt ++dir test \
    ~/tf_deep/tf_deep/scripts/run_deep_autoencoder.py Main \
    --n_input 784 \
    --layers 500 250 10 \
    --encoder_act_list relu relu relu \
    --decoder_act_list sigmoid sigmoid sigmoid \
    --loss cross_entropy \
    --batch_size 500 \
    --learning_rate 0.01 \
    --n_epoch 10 \
    --tied_weight True \
    --batch_normalization True \
    --seed 1 \
    --memory_fraction 0.30 \
    --tensorboard_verbose 0 \
    --logdir tensorboard_dir \
    --trainX ~/tf_deep/data/mnist_training_images.npy \
    --testX ~/tf_deep/data/mnist_test_images.npy \
    --validX ~/tf_deep/data/mnist_validation_images.npy

# TensorBoardの起動
# 起動後ブラウザからTensorBoardにアクセス
# ホストのIPアドレスが192.168.xxx.yyy, コンテナ起動時に指定したTensorBoard用のポート番号がzzzの場合
# ブラウザにhttp://192.168.xxx.yyy:zzzと入力
#
# CNN用のTensorBoardが起動している場合，停止しておく
$ pkill tensorboard
$ tensorboard --logdir=~/tf_deep/tf_deep/data/results/run_deep_autoencoder.Main/test/output_files/tensorboard_dir
```
