# -*- coding:utf-8 -*-
from math import pow
import numpy as np
from exp_wrapper import template
from argparse import ArgumentDefaultsHelpFormatter
from tensorflow.examples.tutorials.mnist import input_data
from os import mkdir
import errno


class Main(template.Main):
    def __init__(self, argv):
        super(Main, self).__init__(argv)

    def execute(self):
        mnist = input_data.read_data_sets('./MNIST', one_hot=True)
        trX = mnist.train.images
        trY = mnist.train.labels
        teX = mnist.test.images
        teY = mnist.test.labels
        vlX = mnist.validation.images
        vlY = mnist.validation.labels

        data_type_list = ['training', 'test', 'validation']
        data_list = [trX, teX, vlX]
        label_list = [trY, teY, vlY]

        for type, data, label in zip(data_type_list, data_list, label_list):
                data_file = 'mnist_%s_images.npy' % type
                label_file = 'mnist_%s_labels.npy' % type
                np.save(arr=data, file=data_file)
                np.save(arr=label, file=label_file)

        return self

    ## コマンドライン引数のパーサを生成するメソッド
    #
    #  コマンドライン引数のパーサを生成するメソッドで,
    #  必要に応じたコマンドライン引数を設定する.
    #  @param self オブジェクト自身に対するポインタ
    #  @return parser パーサオブジェクト
    def make_parser(self):
        parser = super(Main, self).make_parser()
        # parserの設定
        parser_decsription = """
        noised data generator.
        """
        parser.description = parser_decsription
        parser.prog = 'noised_data_generator'
        parser.formatter_class = ArgumentDefaultsHelpFormatter

        return parser


if __name__ == '__main__':
    import sys
    obj = Main(sys.argv[1:])
    template.main(obj)
